package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    HashMap<String, Double> books = new HashMap<>( Map.of(
            "1",10.0,
            "2",45.0,
            "3",20.0,
            "4",35.0,
            "5",50.0
            ) );

    double getBookPrice(String isbn){
        try {
            return books.get(isbn);
        }catch (Exception ignored){}
        return 0.0;
    }
}
